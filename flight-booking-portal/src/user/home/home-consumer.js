import React from "react";
import { Formik, Form } from "formik"; // field will help to maintain and sync with formik state  is used with formik submit function
import * as Yup from 'yup';
import 'date-fns'
import DateFnsUtils from '@date-io/date-fns';
import { makeStyles } from '@material-ui/core/styles';
import {
    Button, Radio, RadioGroup, FormLabel, InputLabel, FormControlLabel, Select, MenuItem, Grid, Container, FormControl
} from '@material-ui/core';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import { UserConsumer } from "../user-context-provider";
import { withRouter } from 'react-router-dom';
import { UserAction } from "../user-action";
import { toast } from 'react-toastify';
import CssBaseline from "@material-ui/core/CssBaseline";


const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
    },
    "@global": {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginBottom: theme.spacing(8),
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
}));

//one can consider this page as deshbord or cockpit 
export const HomePage = withRouter(({ history }) => {

    const notify = (httpResp) => {
        if (isflightBooked) {
            toast.success("Your Flight Is Booked !", {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000 
            });
        }
        else if(isflightBookedError) {

            toast.error(httpResp, {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000 
            });
        }
    }


    const classes = useStyles();
    const cityList = ['Ahmedabad', 'New Delhi', 'Mumbai', 'Bangalore', 'Pune', 'Goa', 'Hyderabad', 'Kolkata', 'Jaipur', 'Lucknow'];
    let isflightBooked = false;
    let isflightBookedError = false;
    return (
        <UserConsumer>
            {({ bookingDetails, updatebookingDetails }) => (
                <Formik
                    initialValues={{
                        tripType: bookingDetails.tripType,
                        fromPlace: bookingDetails.fromPlace,
                        toPlace: bookingDetails.toPlace,
                        travelDate: bookingDetails.travelDate,
                        returnDate: bookingDetails.returnDate,
                        adultsCount: bookingDetails.adultsCount,
                        childrenCount: bookingDetails.childrenCount,
                        infantsCount: bookingDetails.infantsCount,
                        travelClass: bookingDetails.travelClass
                    }}
                    validationSchema={Yup.object().shape({
                    })}
                    onSubmit={async (values, { props, setSubmitting }) => {
                        let bookingHttpResponse = null;
                        if (localStorage.getItem('IsUserAuthenticated')) {
                            if (bookingDetails.tripType === "one-way") {
                                bookingDetails.returnDate = "";
                            }
                            //alert('booking Submit Called!! :-)\n\n' + JSON.stringify(bookingDetails, null, 2));
                            bookingHttpResponse = await UserAction('BOOKFLIGHT', bookingDetails);
                            if (bookingHttpResponse && bookingHttpResponse.success) {
                                isflightBooked = true;
                                notify();
                                history.push("/bookings");
                            }
                            else {
                                isflightBookedError = true;
                                notify(bookingHttpResponse.data);
                            }
                        } else {
                            history.push("/login");
                        }
                    }}
                    render={({ errors, status, touched }) => (
                        <Container component="main" maxWidth="sm">
                            <CssBaseline />
                            <div className={classes.paper}>
                                {/* <Paper variant="outlined" className={classes.Paper}> */}
                                <Form className={classes.Paper}>
                                    {/* {isflightBooked ? <div> Flight Is Booked for more info go to yor bookings tab </div> : null}
                                    {isflightBookedError ? <div> Some Error occure please try again </div> : null} */}
                                    <Grid container spacing={1}>
                                        <Grid item xs={12}>
                                            <FormControl component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">Flight Type</FormLabel>
                                                <RadioGroup
                                                    //aria-label="flightType"
                                                    name="flightType"
                                                    //className={classes.group}
                                                    value={bookingDetails.tripType}
                                                    aria-label="tripType"
                                                    //onChange={formik.handleChange}
                                                    onChange={event => { updatebookingDetails('tripType', event.target.value) }}
                                                    row>
                                                    <FormControlLabel value="one-way" control={<Radio />} label="One way" />
                                                    <FormControlLabel value="two-way" control={<Radio />} label="Round trip" />
                                                </RadioGroup>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">From Place</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.fromPlace}
                                                    onChange={event => { updatebookingDetails('fromPlace', event.target.value) }}
                                                >
                                                    {cityList.map((element, index) => {
                                                        return (<MenuItem key="fromCity_{index}" value={element}>{element}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">From Place</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.fromPlace}
                                                    onChange={event => { updatebookingDetails('fromPlace', event.target.value) }}
                                                    label="FromPlace"
                                                >
                                                    {cityList.map((element, index) => {
                                                        return (<MenuItem key={`fromCity_+${index}`} value={element}>{element}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl>



                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">To Place</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.toPlace}
                                                    onChange={event => { updatebookingDetails('toPlace', event.target.value) }}
                                                >
                                                    {cityList.map((element, index) => {
                                                        return (<MenuItem key={'toCity_' + index} value={element}>{element}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">To Place</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.toPlace}
                                                    onChange={event => { updatebookingDetails('toPlace', event.target.value) }}
                                                    label="ToPlace"
                                                >
                                                    {cityList.map((element, index) => {
                                                        return (<MenuItem key={`toCity_+${index}`} value={element}>{element}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={12} sm={6}>
                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                    <FormLabel component="legend">Travel Date</FormLabel>
                                                    <KeyboardDatePicker
                                                        // label="ToPlace"
                                                        disablePast
                                                        disableToolbar
                                                        variant="inline"
                                                        format="dd-MM-yyyy"
                                                        margin="normal"
                                                        id="date-picker-inline"
                                                        //onChangeRaw={(e)=>{ e.preventDefault(); }}
                                                        //label="Date picker inline"
                                                        value={bookingDetails.travelDate}
                                                        //onChange={formik.handleChange}
                                                        onChange={date => { updatebookingDetails('travelDate', date) }}
                                                        KeyboardButtonProps={{
                                                            'aria-label': 'change date',
                                                        }}
                                                    />
                                                </MuiPickersUtilsProvider>
                                            </FormControl>
                                        </Grid>

                                        <Grid item xs={12} sm={6} >
                                            {bookingDetails.tripType === "two-way" ?
                                                <FormControl variant="outlined" className={classes.formControl}>
                                                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                                        <FormLabel component="legend">Return Date</FormLabel>
                                                        <KeyboardDatePicker
                                                            disablePast
                                                            disableToolbar
                                                            variant="inline"
                                                            format="dd-MM-yyyy"
                                                            margin="normal"
                                                            id="date-picker-inline"
                                                            //label="Date picker inline"
                                                            value={bookingDetails.returnDate}
                                                            onChange={date => { updatebookingDetails('returnDate', date) }}
                                                            KeyboardButtonProps={{
                                                                'aria-label': 'change date',
                                                            }}
                                                        />
                                                    </MuiPickersUtilsProvider>
                                                </FormControl>
                                                : null}
                                        </Grid>

                                        <Grid item xs={6} sm={3}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                              
                                                <FormLabel component="legend">adults</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.adultsCount}
                                                    onChange={event => { updatebookingDetails('adultsCount', event.target.value) }}
                                                >
                                                    {[...Array(10)].map((x, i) => {
                                                        return (<MenuItem key={'toCity_' + i + 1} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">Adults</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.adultsCount}
                                                    onChange={event => { updatebookingDetails('adultsCount', event.target.value) }}
                                                    label="Adults"
                                                >
                                                    {[...Array(7)].map((x, i) => {
                                                        return (<MenuItem key={'toChildren_' + i} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl>



                                        </Grid>
                                        <Grid item xs={6} sm={3}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                               
                                                <FormLabel component="legend">children</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.childrenCount}
                                                    onChange={event => { updatebookingDetails('childrenCount', event.target.value) }}
                                                >
                                                    {[...Array(7)].map((x, i) => {
                                                        return (<MenuItem key={'toCity_' + i} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">Children</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.childrenCount}
                                                    onChange={event => { updatebookingDetails('childrenCount', event.target.value) }}
                                                    label="Children"
                                                >
                                                    {[...Array(7)].map((x, i) => {
                                                        return (<MenuItem key={'toChildren_' + i} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl>


                                        </Grid>
                                        <Grid item xs={6} sm={3}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">infants</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.infantsCount}
                                                    onChange={event => { updatebookingDetails('infantsCount', event.target.value) }}
                                                >
                                                    {[...Array(7)].map((x, i) => {
                                                        return (<MenuItem key={'toCity_' + i} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">Infants</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.infantsCount}
                                                    onChange={event => { updatebookingDetails('infantsCount', event.target.value) }}
                                                    label="Infants"
                                                >
                                                    {[...Array(7)].map((x, i) => {
                                                        return (<MenuItem key={'toInfants_' + i} value={i}>{i}</MenuItem>)
                                                    })}
                                                </Select>
                                            </FormControl>
                                        </Grid>
                                        <Grid item xs={6} sm={3}>
                                            {/* <FormControl component="fieldset" className={classes.formControl}>
                                                <FormLabel component="legend">Travel class</FormLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={bookingDetails.travelClass}
                                                    onChange={event => { updatebookingDetails('travelClass', event.target.value) }}
                                                >
                                                    <MenuItem value={'economy'}>Economy class</MenuItem>
                                                    <MenuItem value={'business'}>Business class</MenuItem>
                                                </Select>
                                            </FormControl> */}

                                            <FormControl variant="outlined" className={classes.formControl}>
                                                <InputLabel id="demo-simple-select-outlined-label">Travel Class</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-outlined-label"
                                                    id="demo-simple-select-outlined"
                                                    value={bookingDetails.travelClass}
                                                    onChange={event => { updatebookingDetails('travelClass', event.target.value) }}
                                                    label="TravelClass"
                                                >
                                                    <MenuItem value={'economy'}>Economy class</MenuItem>
                                                    <MenuItem value={'business'}>Business class</MenuItem>
                                                </Select>
                                            </FormControl>


                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl component="fieldset" className={classes.formControl}>
                                                {/* <Button type="submit" fullWidth variant="raised" color="primary">Submit</Button> */}
                                                <Button type="submit" fullWidth variant="contained" color="primary">
                                                    Book
                                                </Button>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Form>
                                {/* </Paper> */}
                            </div>
                        </Container>
                    )
                    }
                />
            )}
        </UserConsumer >
    )
})
