import React from 'react';
import { MyPlacePickerHoc } from "../../hoc/withSourceDestinationPicker";

export const testHocForCustomSelection = () => {

    // user's control where user can manuplate change event as they want
    const handelChangeEvent = () => {
        console.log('[from samplehoc js] where user can use change event as par their need ');
    }

    const config = {
        fromId: 'formDestination',
        toId: 'toDestination',
        fromTitle: 'From',
        toTitle: 'To',
        fromSelectOptions: ['Ahmedabad', 'New Delhi', 'Mumbai', 'Bangalore', 'Pune', 'Goa', 'Hyderabad', 'Kolkata', 'Jaipur', 'Lucknow'],
        toSelectOptions: ['Ahmedabad', 'New Delhi', 'Mumbai', 'Bangalore', 'Pune', 'Goa', 'Hyderabad', 'Kolkata', 'Jaipur', 'Lucknow'],
        displayType: 'inline',
        onChangeSelection: handelChangeEvent
    }

    return (
        <MyPlacePickerHoc config={config}> </MyPlacePickerHoc>
    )
}