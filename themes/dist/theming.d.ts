/// <reference types="react" />
export declare const ThemeProvider: import("react").ComponentType<{
    theme?: string | undefined;
}>, withTheme: <Props extends {
    theme: string;
}, C>(WrappedComponent: (import("react").ComponentClass<Props, any> & C) | (import("react").FunctionComponent<Props> & C)) => (import("react").ComponentClass<Pick<Props, Exclude<keyof Props, "theme">> & {
    theme?: string | undefined;
}, any> & import("@callstack/react-theme-provider/typings/hoist-non-react-statics").NonReactStatics<(import("react").ComponentClass<Props, any> & C) | (import("react").FunctionComponent<Props> & C), {}>) | (import("react").FunctionComponent<Pick<Props, Exclude<keyof Props, "theme">> & {
    theme?: string | undefined;
}> & import("@callstack/react-theme-provider/typings/hoist-non-react-statics").NonReactStatics<(import("react").ComponentClass<Props, any> & C) | (import("react").FunctionComponent<Props> & C), {}>), useTheme: (overrides?: string | undefined) => string;
