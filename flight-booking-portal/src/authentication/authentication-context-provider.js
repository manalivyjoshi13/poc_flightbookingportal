import React, { Component, createContext } from "react";

export const AuthContext = new createContext({
    isAuthenticated: false,
    setAuthenticated: () => { },
});

export class AuthenticationContextProvider extends Component {

    updateAuthState = () => {
        this.setState({ isAuthenticated: localStorage.getItem('IsUserAuthenticated') });
    }

    setRegistartionDetails = (field, value) => {
        let registrationDeatilsClone  =  {...this.state.registrationDetails} //cloning object
        registrationDeatilsClone[field] = value;
        this.setState({ registrationDetails: registrationDeatilsClone });
    };

    constructor(props) {
        super(props);
        this.state = {
            isAuthenticated: false,
            setAuthenticated: this.updateAuthState,
            registrationDetails:
            {
                "email": "",
                "password": "",
                "firstName": "",
                "lastName": "",
                "mobile": "",
                "gender": "Female"

            },
            updateRegistrationDetails: this.setRegistartionDetails,
        };
    }

    render() {
        return (
            <AuthContext.Provider value={this.state} >
                {this.props.children}
            </AuthContext.Provider >
        )
    }
}

export const AuthConsumer = AuthContext.Consumer;