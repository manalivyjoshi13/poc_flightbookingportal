import { Login } from "./login/login";
import { Registration } from "../authentication/registration/registration";
import { testHocForCustomSelection } from "../authentication/sampleHOC/sampleHOC";
import { TestTextFieldHOC } from "../authentication/sampleHOC/testTextFieldHOC";

export const authRouts = [
    {
        path: '/login',
        exact: true,
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        exact: true,
        name: 'Register',
        component: Registration
    },
    {
        path: '/hocSelectionFromTo',
        exact: true,
        name: 'SourceDestination',
        component: testHocForCustomSelection
    },
    {
        path: '/hocInputText',
        exact: true,
        name: 'TextField',
        component: TestTextFieldHOC
    },
]

