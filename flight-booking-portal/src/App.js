import React, { useState } from 'react';
//import logo from './logo.svg';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import './App.scss';
import { appRoutes } from "../src/App-routes";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { HeaderConsumer } from './common/component/header-consumer';
import { FooterConsumer } from './common/component/footer-consumer';
import { UserContextProvider } from "./user/user-context-provider";
import { AuthenticationContextProvider } from "./authentication/authentication-context-provider";
//all the below imports are for theme custom library 
import { DarkTheme, LightTheme, ThemeProvider } from "@manali-web/theme";
import '@manali-web/theme/dist/css/theme.css';

function App() {

  //use to set defult them and update after user selection 
  const [theme, setTheme] = useState(LightTheme);

  toast.configure({
    autoClose: 1000,
    draggable: false,
    //etc you get the idea
  });

  function handleThemeChange(changeEvt) {
    if (theme === "lightTheme") {
      setTheme(DarkTheme);
    } else {
      setTheme(LightTheme);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <div className={`${theme}`}>
        <Router>
          <AuthenticationContextProvider>
            <HeaderConsumer onChangeTheme={handleThemeChange} currentTheme={theme}></HeaderConsumer>
            <UserContextProvider>
              <Switch>
                {
                  appRoutes.map(r =>
                    <Route key={r.path}
                      path={r.path}
                      exact={r.exact}
                      component={r.component}
                      name={r.name} />
                  )
                }
              </Switch>
            </UserContextProvider>
            <FooterConsumer></FooterConsumer>
          </AuthenticationContextProvider>
        </Router>
        <ToastContainer />
      </div>
    </ThemeProvider>
  );
}

export default App;
