import React, { useContext, useEffect } from "react";
import { AppBar, Toolbar, IconButton, Typography, Button, Grid , Switch  } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

import BrightnessLowIcon from '@material-ui/icons/BrightnessLow';
import Brightness3Icon from '@material-ui/icons/Brightness3';

import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { AuthConsumer } from "../../authentication/authentication-context-provider";
import { withRouter } from 'react-router-dom';

import { AuthContext } from "../../authentication/authentication-context-provider";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    displayInline: {
        display: "inline-block",
        margin: "auto",
        padding: "3px",

    },
    backButton: {
        float: "left",
        //background: "Red",
        width: "35%",
        //height: 280px; 
    },
    userName: {
        float: "left",
        //background: "Green",
        width: "50%",
        //height:280px; 
    },
    menuProfile: {
        float: "right",
        //background: "blue",
        width: "15%",
        //height:280px; 
    }
}));


export const HeaderConsumer = withRouter(({ history, onChangeTheme, currentTheme }) => {

    console.log(currentTheme);
    const contextValue = useContext(AuthContext);
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const isMenuOpen = Boolean(anchorEl);
    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };
    const handleMenuClose = () => {
        setAnchorEl(null);
    };

    const handleLogout = () => {
        // remove from key from local strorage
        localStorage.clear();
        contextValue.setAuthenticated();
        history.push("/home");
    }

    const menuId = 'primary-search-account-menu';
    const renderMenu = (
        <Menu
            anchorEl={anchorEl}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            id={menuId}
            keepMounted
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
            open={isMenuOpen}
            onClose={handleMenuClose}
        >
            <MenuItem onClick={() => history.push("/bookings")}>Bookings</MenuItem>
            <MenuItem onClick={handleLogout}>Logout</MenuItem>

        </Menu>
    );

    const handleClick = (event, name) => {
        if (name === "login") {
            history.push("/login");
        }
        else if (name === "Registration") {
            history.push("/register");
        }
        else {
            history.push("/home");
        }
    }

    const currentUrl = window.location.href;
    let headerAuthenticated = null;
    let header = null;
    if ((currentUrl.indexOf('/login') > - 1) || (currentUrl.indexOf('/register') > -1)) {
        header = <div>
            <Button color="inherit" onClick={(e) => { handleClick(e, "back") }}>Back</Button>
        </div>;
    }
    else {

        if ((currentUrl.indexOf('/bookings') > - 1)) {
            headerAuthenticated = <div>
                <div className={classes.backButton}>
                    <Button color="inherit" onClick={(e) => { handleClick(e, "back") }}>Back</Button>
                </div>
                <div className={classes.userName}>  <Typography>
                    Welcome  {localStorage.getItem("UserName") ? localStorage.getItem("UserName") : null}
                </Typography></div>
                <div className={classes.menuProfile}>
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        aria-controls={menuId}
                        aria-haspopup="true"
                        onClick={handleProfileMenuOpen}
                        color="inherit"
                    >
                        <AccountCircle />
                    </IconButton>
                </div>
            </div>;
        }
        else {
            headerAuthenticated = <div>
                <div className={classes.userName}>
                    <Typography>
                        Welcome {localStorage.getItem("UserName") ? localStorage.getItem("UserName") : null}
                    </Typography>
                </div>
                <div className={classes.menuProfile}>
                    <IconButton
                        edge="end"
                        aria-label="account of current user"
                        aria-controls={menuId}
                        aria-haspopup="true"
                        onClick={handleProfileMenuOpen}
                        color="inherit"
                    >
                        <AccountCircle />
                    </IconButton>
                </div>
            </div>;
        }
        header = <div>
            {/* <Link color="inherit" to="/login"> Click to login </Link>
             <Link color="inherit" to="/register"> Click to register </Link> */}
            <Button color="inherit" onClick={(e) => { handleClick(e, "login") }}>Login </Button>
            <Button color="inherit" onClick={(e) => { handleClick(e, "Registration") }}>Registration</Button>
        </div >;

    }



    useEffect(() => {
        contextValue.setAuthenticated();
    }, []);


    return (
        //return any one of them with login/ authentication or without
        <AuthConsumer>
            {({ isAuthenticated }) => (
                <header>
                    <div className={classes.root}>
                        <AppBar position="static">
                            <Toolbar>
                                <Typography variant="h6" className={classes.title}>
                                    Book Your Flights
                               </Typography>
                                <Typography component="div">
                                    <Grid component="label" container alignItems="center" spacing={1}>
                                        <Grid item><Brightness3Icon /></Grid>
                                        <Grid item>
                                            <Switch defaultChecked onChange={e => onChangeTheme(e)} color="default" />
                                        </Grid>
                                        <Grid item><BrightnessLowIcon /></Grid>
                                    </Grid>
                                </Typography>
                                {/* <select onChange={e => onChangeTheme(e.target.value)}>
                                    {themes.map((themeName) => {
                                        return (
                                            <option key={themeName} value={themeName}>
                                                {themeName}
                                            </option>
                                        );
                                    })}
                                </select> */}
                                {isAuthenticated ? headerAuthenticated : header}
                            </Toolbar>
                        </AppBar>
                        {isAuthenticated ? renderMenu : null}
                    </div>
                </header>
            )}
        </AuthConsumer>
    )
})