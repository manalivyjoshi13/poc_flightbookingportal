import React from "react";
import { Formik, Form } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import {
    TextField, Button, Grid, Container, FormControl, Typography
} from '@material-ui/core';
import { AuthAction } from "../auth-action";
import { AuthConsumer } from "../authentication-context-provider";
import { withRouter } from 'react-router-dom';
import { toast } from 'react-toastify';

const useStyles = makeStyles(theme => ({
    formControl: {
        margin: theme.spacing(1),
    },
    // Paper: {
    //     background: "#3f51b536",
    // },

    "@global": {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginBottom : theme.spacing(20),
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },

}));

export const Login = withRouter(({ history, location }) => {
    const classes = useStyles();
    let loginError = false;
    let loginSuccess = false;

    toast.configure({
        autoClose: 1000,
        draggable: false,
        //etc you get the idea
    });

    //for toster 
    const notify = () => {
        if (loginSuccess) {
            toast.success("You Are Logged in Successfully !", {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000 
            });
        }
        else if (loginError) {
            toast.error("Username Or Password Wrong !", {
                position: toast.POSITION.TOP_RIGHT,
                autoClose: 2000 
            });
        }
    }

    return (
        <AuthConsumer>
            {({ setAuthenticated }) => (
                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    validationSchema={Yup.object().shape({
                        email: Yup.string()
                            .email('Email is invalid')
                            .required('Email is required'),
                        password: Yup.string()
                            .required('Password is required'),
                    })}
                    onSubmit={async (submitedValues, { props, setSubmitting, resetForm }) => {
                        let loginHttpResponse = null;
                        //alert('Login Submit Called!! :-)\n\n' + JSON.stringify(submitedValues, null, 2));
                        loginHttpResponse = await AuthAction('LOGIN', submitedValues);
                        if (loginHttpResponse.success) {
                            localStorage.setItem('UserName', loginHttpResponse.data.name)
                            localStorage.setItem('IsUserAuthenticated', true)
                            setAuthenticated();
                            resetForm({});// to reset the  error touch and submit state
                            loginSuccess = true;
                            notify();
                            history.push("/home");
                        }
                        else {
                            loginError = true;
                            notify();
                        }
                    }}
                    render={({ errors, status, touched, handleChange }) => (
                        <Container component="main" maxWidth="xs">
                            {/* <CssBaseline /> */}
                            <div className={classes.paper}>
                                {/* <Paper variant="outlined" className={classes.Paper}> */}
                                <Form className={classes.form}>
                                    <Grid container spacing={2}>
                                        <Grid item xs={12}>
                                            {/* <FormLabel component="legend">Login</FormLabel> */}
                                            <Typography component="h1" variant="h5">
                                                Login
                                            </Typography>
                                        </Grid>
                                        <Grid item xs={12}>
                                            {/* <TextField id="outlined-basic" label="Email ID" variant="outlined" /> */}
                                            {/* <Field name="email" type="email" />
                                            {errors.email && touched.email ? <div>{errors.email}</div> : null} */}

                                            <TextField
                                                error={errors.email && touched.email}
                                                variant="outlined"
                                                fullWidth
                                                onChange={handleChange}
                                                id="email"
                                                label="Email Address"
                                                name="email"
                                                autoComplete="email"
                                                helperText={
                                                    errors.email && touched.email ? errors.email : null
                                                }
                                            />


                                        </Grid>
                                        <Grid item xs={12}>
                                            {/* <TextField id="outlined-basic" label="password" variant="outlined" /> */}
                                            {/* <Field name="password" type="password" />
                                            {errors.password && touched.password ? <div>{errors.password}</div> : null} */}

                                            <TextField
                                                error={errors.password && touched.password}
                                                variant="outlined"
                                                fullWidth
                                                onChange={handleChange}
                                                name="password"
                                                label="Password"
                                                type="password"
                                                id="password"
                                                autoComplete="current-password"
                                                helperText={
                                                    errors.password && touched.password
                                                        ? errors.password
                                                        : null
                                                }
                                            />

                                        </Grid>
                                        <Grid item xs={12}>
                                            <FormControl component="fieldset" className={classes.formControl}>
                                                {/* <Button type="submit" fullWidth variant="outlined" color="primary">Login</Button> */}
                                                <Button type="submit" variant="contained" color="primary">
                                                    Login
                                                </Button>
                                            </FormControl>
                                        </Grid>
                                    </Grid>
                                </Form>
                                {/* </Paper> */}
                            </div>
                        </Container>
                    )}>
                </Formik>
            )}
        </AuthConsumer>
    )
})

