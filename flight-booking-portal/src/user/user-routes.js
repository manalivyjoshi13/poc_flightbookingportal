import { HomePage } from "../user/home/home-consumer";
import { BookingList } from "../user/list-booking/list-booking-consumer";

export const userRouts = [
    {
        path: '/',
        exact: true,
        name: 'Home',
        component: HomePage
    },
    {
        path: '/home',
        exact: true,
        name: 'Home',
        component: HomePage
    },
    {
        path: '/bookings',
        exact: true,
        name: 'Bookings',
        component: BookingList
    },
]
