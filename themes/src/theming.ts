import { createTheming } from "@callstack/react-theme-provider";
import { Theme,LightTheme } from './themeType';

export const { ThemeProvider, withTheme, useTheme } = createTheming<Theme>(LightTheme);




