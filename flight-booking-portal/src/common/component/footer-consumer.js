import React from "react";
import { AppBar, Toolbar,Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        // position: "relative",
        // bottom: 0,
        // width:"100%"
    },
    title: {
        flexGrow: 1,
    },
    // footer: {
    //     position: "fixed",
    //     bottom: 0,
    //     width: "initial",
    // }
}));
export const FooterConsumer = () => {
    const classes = useStyles();
    return (
        //doesn't need any authentication
        <footer>
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="subtitle1" className={classes.title}>
                            @copyright bookyourdreamtour.com
                        </Typography>
                    </Toolbar>
                </AppBar>
            </div>
        </footer>
    )
} 