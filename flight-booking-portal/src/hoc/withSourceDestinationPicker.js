import React from 'react';

//for custom styling 
let displayTypeInline = {
    display: 'flex'
}

//jsx for select inputs
const myCustomPlaceSelector = ({ config, handelChange }) => (
    < div style={config.displayType === 'inline' ? displayTypeInline : null}  >
        <div style={{ margin: 20 }}>
            <label htmlFor={config.fromId} style={{ margin: 10 }}>{config.fromTitle}</label>
            <select id={config.fromId} onChange={(e) => handelChange(e)}>
                {config.fromSelectOptions.map((element, index) => (
                    <option key={`toCity- + ${element}`} value={element}>{element}</option>
                ))}
            </select>
        </div>
        <div style={{ margin: 20 }}>
            <label htmlFor={config.toId} style={{ margin: 10 }}>{config.toTitle}</label>
            <select style = {{ maxHeight : 100 , borderblock: 5}} id={config.toId} onChange={(e) => handelChange(e)}>
                {config.toSelectOptions.map((element, index) => (
                    <option key={`toFrom- + ${element}` } value={element}>{element}</option>
                ))}
            </select>
        </div>
    </div >
)

//higher order component for comman busniess logic 
const sampleHOC = (WrappedComponent) => {

    // And return a new anonymous component
    return class extends React.Component {

        constructor(props) {
            super(props);
            this.state = { Defultconfig: { ...props } }; //create a clone using  spread operator
            this.stateCopy = JSON.parse(JSON.stringify(this.state)); //deep copy of state
        }

        handelChange = (e) => {
            // if from id is same as selected value from select
            // remove selected value from to state
            if (e.target.id === this.state.Defultconfig.config.fromId) {
                //filter the value which is selected in fom selector
                // filter array for numbers in a range
                this.state.Defultconfig.config.toSelectOptions = this.stateCopy.Defultconfig.config.toSelectOptions.filter(function (option) {
                    return option !== e.target.value
                })
                this.setState({ Defultconfig: this.state.Defultconfig })
            }
            return this.state.Defultconfig.config.onChangeSelection();//give event back to user for there changes
        }
        render() {
            return <WrappedComponent config={this.state.Defultconfig.config} handelChange={this.handelChange} />;
        }
    }
};

//export for use hoc every where in app or another app
export const MyPlacePickerHoc = sampleHOC(myCustomPlaceSelector);