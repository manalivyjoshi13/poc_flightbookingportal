export type Theme = string;
export  const LightTheme = 'lightTheme';
export const DarkTheme = 'darkTheme';