import React, { useContext, useEffect } from "react";
import { UserConsumer } from "../user-context-provider";
import { UserContext } from "../user-context-provider";
import { UserAction } from "../user-action";
import { withRouter } from 'react-router-dom';
//used for table structure 
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {
    Button
} from '@material-ui/core';

// import { Formik, Form, Field } from "formik";
// import { Button, LinearProgress } from '@material-ui/core';
// import { TextField } from 'formik-material-ui';

// async function getBookingDeatils() {
//     let error = false;
//     var errorMessage = null;

//     var list = null;
//     alert('getBookingDeatils');
//     let getBookingHttpResponse = null;
//     if (localStorage.getItem('IsUserAuthenticated')) {
//         getBookingHttpResponse = await UserAction('BOOKINGDETAILS', {});
//         if (getBookingHttpResponse && getBookingHttpResponse.success) {
//             return getBookingHttpResponse.data
//             //  return usercontextValue.bookingList;
//         }
//         else {
//             error = true;
//             return [];
//         }
//     } else {
//         //history.push("/home");
//     }
// }


const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});


export const BookingList = withRouter(({ history }) => {

    const classes = useStyles();
    const usercontextValue = useContext(UserContext);

    useEffect(() => {
        setTimeout(() => {
            UserAction('BOOKINGDETAILS', {}).then((response) => {
                if (response.success) {
                    usercontextValue.setBookingList(response.data);
                }
                else {
                    // error = true;
                    usercontextValue.setBookingList([]);
                }
            }).catch((error) => {
            });
        }, 100);
        return (() => {
            console.log("cleanup similar to componentWillUnmount");
        });
    }, []);


    //delete functionality 
    const manageFlightCancle = async (event, id) => {

        let cancleRes = await UserAction('CANCLEDETAILS', {
            "bookingId": id
        });

        if (cancleRes.success) {
            let getLatestBooking = await UserAction('BOOKINGDETAILS', {});
            if (getLatestBooking.success) {
                usercontextValue.setBookingList(getLatestBooking.data);
            }
            else {
                usercontextValue.setBookingList([]);
            }
        }
        // let bookinListClone = [...usercontextValue.bookingListData];
        // bookinListClone.splice(id, 1);
        // //update the value in actual array
        // usercontextValue.setBookingList(bookinListClone);
    }

    return (

        // <h1> hello </h1>
        <UserConsumer>
            {({ bookingListData }) => (
                <div>{bookingListData.length > 0 ?
                    // <div>{
                    //     bookingListData.map((element, index) => {
                    //         return <div key={element._id}>
                    //             <lable> from place : {element.fromPlace} to place : {element.toPlace} </lable>
                    //             <lable> Date : {element.travelDate} Status : {element.status} </lable>
                    //             <button onClick={(e) => { manageFlightCancle(e, element._id) }}> Cancel </button>
                    //         </div>
                    //     })
                    // }</div>


                    <TableContainer component={Paper}>
                        <Table className={classes.table} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    {/* <TableCell>Dessert (100g serving)</TableCell> */}
                                    <TableCell align="center">From Place</TableCell>
                                    <TableCell align="center">To Place</TableCell>
                                    <TableCell align="center">Date</TableCell>
                                    <TableCell align="center">Status</TableCell>
                                    <TableCell align="center">Cancel Flight</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {bookingListData.map(row => (
                                    <TableRow key={row._id}>
                                        {/* <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell> */}
                                        <TableCell align="center">{row.fromPlace}</TableCell>
                                        <TableCell align="center">{row.toPlace}</TableCell>
                                        <TableCell align="center">{row.travelDate}</TableCell>
                                        <TableCell align="center">{row.status}</TableCell>
                                        {row.status === "booked" ? <TableCell align="center">{<Button variant="contained" onClick={(e) => { manageFlightCancle(e, row._id) }}> Cancel </Button>}</TableCell> : <TableCell align="center"></TableCell>}
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>


                    : <h1> No Bookings Are Found  </h1>}
                </div>
            )}
        </UserConsumer >
    )
}) 