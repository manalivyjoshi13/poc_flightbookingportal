export const userActions = {
    BOOK_FLIGHT : "/flights/bookFlight?",
    GET_BOOKING_DATA : "/flights/getBookingsData",
    CANCEL_FLIGHT :"/flights/cancelFlight",
}