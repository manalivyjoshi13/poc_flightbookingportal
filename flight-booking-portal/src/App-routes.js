import { authRouts } from "./authentication/auth-routes";
import { userRouts } from "./user/user-routes";

let margeAppRouts = [];
margeAppRouts.push(authRouts);
margeAppRouts.push(userRouts);

export const appRoutes = [].concat.apply([], margeAppRouts);



