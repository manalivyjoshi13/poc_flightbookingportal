// used for http call
import axios from "axios";
//where all the action methods are defined
import { userActions } from "../user/actions";
//where the basic app config constants define
import { appConfig } from "../config";


export const UserAction = async (actionType, data) => {

    switch (actionType) {
        case 'BOOKFLIGHT': return await bookFlight(data);
        //break;
        case 'BOOKINGDETAILS': return await getBookingsData();
        //break;
        case 'CANCLEDETAILS': return await cancelFlight(data);
        //break;
        default:
            break;
    }
}

const bookFlight = async (requestData) => {
    let bookFlightResp = null;
    try {
        bookFlightResp = await axios.post(appConfig.endPointApi + userActions.BOOK_FLIGHT, requestData);
    } catch (error) {
        bookFlightResp = error;
    }
    return bookFlightResp;
}
const getBookingsData = async () => {
    let getBookingsResp = null;
    try {
        getBookingsResp = await axios.post(appConfig.endPointApi + userActions.GET_BOOKING_DATA, null);
    } catch (error) {
        getBookingsResp = error;
    }
    return getBookingsResp;
}
const cancelFlight = async (requestData) => {
    let cancelFlightResp = null;
    try {
        cancelFlightResp = await axios.post(appConfig.endPointApi + userActions.CANCEL_FLIGHT, requestData);
    } catch (error) {
        cancelFlightResp = error;
    }
    return cancelFlightResp;
}




