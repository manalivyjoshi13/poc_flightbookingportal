import React, { Component } from "react";

const WrapperComponent = (props: any) => {
    return <div>
        {props.lableText ? <label style= {{ margin : 5 }} htmlFor={props.id}>{props.lableText}</label> : null}
        <input style= {{ margin : 5 }} type={props.type} id={props.id} name={props.name} defaultValue={props.value} onChange={(e) => props.onChange(e)} ></input>
    </div>;
}

interface property {
    id: string;
    name: string;
    type: string;
    value?: string;
    lableText?: string;
    onChange?: any
}

const WithCustomTextField = (WrapperComponent: any) => {
    //return annonymous class :
    return class extends Component<property, void> {

        constructor(props: any) {
            super(props);
        }

        render() {
            //passing props/properties which is defined by user for component
            return <WrapperComponent {...this.props} ></WrapperComponent>
        }
    }
}

export const HOCTextField = WithCustomTextField(WrapperComponent);


