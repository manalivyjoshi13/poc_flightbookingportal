// used for http call
import axios from "axios";
//where all the action methods are defined
import { authActions } from "../authentication/actions";
//where the basic app config constants define
import { appConfig } from "../config";

export const AuthAction = async (actionType, data) => {

    switch (actionType) {
        case 'LOGIN': return await login(data);
        //break;
        case 'RGISTRATION': return await registration(data);
        //break;
        default:
            break;
    }
}

const login = async (requestData) => {
    let loginResp = null;
    try {
        loginResp = await axios.post(appConfig.endPointApi + authActions.USER_LOGIN, requestData);
    } catch (error) {
        loginResp = error;
    }
    return loginResp;
}
const registration = async (requestData) => {
    let registrationResp = null;
    try {
        registrationResp = await axios.post(appConfig.endPointApi + authActions.USER_REGISTRATION, requestData);
    } catch (error) {
        registrationResp = error;
    }
    return registrationResp;
}




