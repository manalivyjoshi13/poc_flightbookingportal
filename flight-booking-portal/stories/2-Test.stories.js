import React from 'react';
import { action } from '@storybook/addon-actions';
import { Button } from '@storybook/react/demo';
import { HOCTextField } from "../../flight-booking-portal/src/hoc/withCustomTextBox";
import { HOCDatePickers } from "../../flight-booking-portal/src/hoc/withCustomDatePickers";
import { MyPlacePickerHoc } from "../../flight-booking-portal/src/hoc/withSourceDestinationPicker";

export default {
    title: 'My Test Components',
    component: Button,
};
const config = {
    fromId: 'formDestination',
    toId: 'toDestination',
    fromTitle: 'From',
    toTitle: 'To',
    fromSelectOptions: ['Ahmedabad', 'New Delhi', 'Mumbai', 'Bangalore', 'Pune', 'Goa', 'Hyderabad', 'Kolkata', 'Jaipur', 'Lucknow'],
    toSelectOptions: ['Ahmedabad', 'New Delhi', 'Mumbai', 'Bangalore', 'Pune', 'Goa', 'Hyderabad', 'Kolkata', 'Jaipur', 'Lucknow'],
    displayType: 'inline',
    //onChangeSelection: handelChangeEvent
}

export const textBox = () =>
    (<HOCTextField id='myCustomTextId' type='text' name='myCustomTextInput'
        onChange={action('clicked')}
        lableText="User Name"></HOCTextField>);

export const placeRangePicker = () => (
    <MyPlacePickerHoc config={config}> </MyPlacePickerHoc>
);

export const dateRangePicker = () => (
    <HOCDatePickers formDateLableText='Depart' toDateLableText='Return'
        fromDateId='DepartDatePickerId' toDateId='ReturnDatePickerId'
        fromDateValue='2020-03-30' toDateValue='2020-03-30'
        fromDatename='DepartDate' toDatename='ReturnDate' onChange={action('clicked')} ></HOCDatePickers>
);
