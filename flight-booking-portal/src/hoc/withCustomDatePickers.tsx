import React from "react";

const WrapperComponent = (props: any) => {
    return <div>
        {props.formDateLableText ?
            <label style={{ margin: 5 }} htmlFor={props.fromDateId}>{props.formDateLableText}</label>
            : null}
        <input style={{ margin: 5 }} type="date" id={props.fromDateId} name={props.fromDatename}
            value={props.fromDateValue} onChange={(e) => props.handelChange(e)} min={props.fromDateValue} ></input>
        {props.toDateLableText ?
            <label style={{ margin: 5 }} htmlFor={props.toDateId}>{props.toDateLableText}</label>
            : null}
        <input style={{ margin: 5 }} type="date" id={props.toDateId} name={props.toDatename}
            value={props.toDateValue} onChange={(e) => props.handelChange(e)} min={props.fromDateValue} ></input>
    </div>;
}

interface property {
    fromDateId?: string;
    toDateId?: string;
    fromDatename?: string;
    toDatename?: string;
    //type: string;
    fromDateValue?: string;
    toDateValue?: string;
    formDateLableText?: string;
    toDateLableText?: string;
    onChange?: any
}

const WithCustomDatePickers = (WrapperComponent: any) => {
    //return annonymous class :
    return class extends React.Component<property, any> {
        //stateCopy: any; // if you want to use and clone in constructer its self then need to declare first
        constructor(props: any) {
            super(props);
            this.state = { Defultconfig: { ...props } }; //create a state using spread operator
            //this.stateCopy = JSON.parse(JSON.stringify(this.state)); //deep copy of state
        }

        handelChange = (e: any) => {
            var stateCopy = { ...this.state.Defultconfig };
            if (e.target.id === this.state.Defultconfig.fromDateId) {
                stateCopy.fromDateValue = e.target.value;
                stateCopy.toDateValue = e.target.value;
            }
            else {
                stateCopy.toDateValue = e.target.value;
            }
            this.setState({ Defultconfig: stateCopy });
            return this.state.Defultconfig.onChange();//give event back to user for there changes
        }

        render() {
            //passing props/properties which is defined by user for component
            return <WrapperComponent {...this.state.Defultconfig} handelChange={this.handelChange}  ></WrapperComponent>
        }
    }
}

export const HOCDatePickers = WithCustomDatePickers(WrapperComponent);


