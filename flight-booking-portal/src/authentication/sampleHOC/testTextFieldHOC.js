import React from "react";
import { HOCTextField } from "../../hoc/withCustomTextBox";
import { HOCDatePickers } from "../../hoc/withCustomDatePickers";

const handleChange = (e) => {
    alert('You Have Entered :---> ' + e.target.value);
}


function getTodayDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

const onChangeEvent = (e) => {
console.log('[Users custom change called]');
}

export const TestTextFieldHOC = () => {
    return (
        <div>
            <div>
                <HOCTextField id='myCustomTextId' type='text' name='myCustomTextInput' onChange={handleChange} lableText="User Name"></HOCTextField>
            </div>
            <div>
                <HOCDatePickers formDateLableText='Depart' toDateLableText='Return'
                    fromDateId='DepartDatePickerId' toDateId='ReturnDatePickerId'
                    fromDateValue={getTodayDate()} toDateValue={getTodayDate()}
                    fromDatename='DepartDate' toDatename='ReturnDate' onChange={(e) => onChangeEvent(e)} ></HOCDatePickers>
            </div>
        </div>
    )
}