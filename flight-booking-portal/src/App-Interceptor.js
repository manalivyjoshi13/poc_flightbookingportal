import axios from "axios";

export const AppIterceptor = () => {

    axios.interceptors.request.use((request) => {
        if (!(request.url.indexOf('login') > -1) && !(request.url.indexOf('register') > -1)) {
            request.headers["x-access-token"] = localStorage.getItem('Access-Token');
        }
        return Promise.resolve(request);
    }, error => {
        console.log(error);
        return Promise.reject(error);
    });

    axios.interceptors.response.use((response) => {
        if(!localStorage.getItem('Access-Token') && response.data.data.token){
            localStorage.setItem('Access-Token', response.data.data.token)
        }
        return Promise.resolve(response.data);
    }, error => {
        console.log(error);
        return Promise.reject(error);
    });

}




