import React, { createContext, Component } from "react";
//import * as React from 'react';
export const UserContext = createContext({
    bookingDetails: {},
    updatebookingDetails: () => { },
    bookingListData: [],
    setBookingList: () => { },
});

export class UserContextProvider extends Component {

    setbookingDetails = (field, value) => {
        let bookingDeatilsClone = { ...this.state.bookingDetails } //cloning object
        bookingDeatilsClone[field] = value;
        this.setState({ bookingDetails: bookingDeatilsClone });
    };

    updateLastestBookingInfo = (bookings) => {
        this.setState({ bookingListData: bookings });
    };


    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         bookingDetails:
    //         {
    //             "tripType": "one-way",
    //             "fromPlace": "Ahmedabad",
    //             "toPlace": "",
    //             "travelDate": new Date(),
    //             "returnDate": new Date(),
    //             "adultsCount": 0,
    //             "childrenCount": 0,
    //             "infantsCount": 0,
    //             "travelClass": "economy"
    //         },
    //         updatebookingDetails: this.setbookingDetails,
    //     };
    // }


    state = {
        bookingDetails:
        {
            "tripType": "two-way",
            "fromPlace": "Ahmedabad",
            "toPlace": "Jaipur",
            "travelDate": new Date(),
            "returnDate": new Date(),
            "adultsCount": 1,
            "childrenCount": 0,
            "infantsCount": 0,
            "travelClass": "economy"
        },
        updatebookingDetails: this.setbookingDetails,
        bookingListData: [],
        setBookingList: this.updateLastestBookingInfo
    };




    render() {
        return (
            <UserContext.Provider value={this.state}>
                {this.props.children}
            </UserContext.Provider >
        )
    }
}

export const UserConsumer = UserContext.Consumer;