export declare type Theme = string;
export declare const LightTheme = "lightTheme";
export declare const DarkTheme = "darkTheme";
