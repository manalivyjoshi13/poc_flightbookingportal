import React from "react";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {
    FormLabel, FormControlLabel, RadioGroup, Radio
} from '@material-ui/core';
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { withRouter } from 'react-router-dom';
import { AuthAction } from "../auth-action";
import { toast } from 'react-toastify';

//for mobile validations
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

let SignupSchema = Yup.object().shape({
    firstName: Yup.string()
        .required('First Name is required'),
    lastName: Yup.string()
        .required('Last Name is required'),
    email: Yup.string()
        .email('Email is invalid')
        .required('Email is required'),
    password: Yup.string()
        .min(6, 'Password must be at least 6 characters')
        .max(20, "Password is too long.")
        .required('Password is required'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password'), null], 'Passwords must match')
        .required('Confirm Password is required'),
    mobile: Yup.string()
        .matches(phoneRegExp, 'Mobile number is not valid')
        .required('Mobile is required')
});

const useStyles = makeStyles(theme => ({
    "@global": {
        body: {
            backgroundColor: theme.palette.common.white
        }
    },
    paper: {
        marginTop: theme.spacing(8),
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginBottom: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: "100%", // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));

export const Registration = withRouter(({ history, location }) => {

    const classes = useStyles();
    let registrationSuccess = false;
    let registrationFail = false;

    const notify = () => {
        if (registrationSuccess) {
            toast.success("Registration Success !", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
        else if (registrationFail) {
            toast.error("Registration Error!", {
                position: toast.POSITION.TOP_RIGHT
            });
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <Formik
                    initialValues={{
                        firstName: "",
                        lastName: "",
                        email: "",
                        password: "",
                        confirmPassword: "",
                        mobile: "",
                        gender: "Female"
                    }}
                    validationSchema={SignupSchema}
                    onSubmit={async (values, { resetForm, initialValues }) => {
                        let resgistrationHttpResponse = null;
                        resgistrationHttpResponse = await AuthAction('RGISTRATION', values);
                        resetForm(initialValues);
                        if (resgistrationHttpResponse && resgistrationHttpResponse.success) {
                            registrationSuccess = true;
                            resetForm({});// to reset the  error touch and submit state
                            notify();
                            history.push("/login");
                        }
                        else {
                            registrationFail = true;
                        }
                    }}
                >
                    {({ errors, handleChange, touched }) => (
                        <Form className={classes.form}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={3}>
                                    <FormLabel component="legend" style={{ paddingTop: 12 }}>Gender</FormLabel>
                                </Grid>
                                <Grid item xs={9}>
                                    <RadioGroup
                                        name="genderType"
                                        //className={classes.group}
                                        defaultValue="Female"
                                        aria-label="genderType"
                                        //onChange={formik.handleChange}
                                        onChange={handleChange}
                                        row>
                                        <FormControlLabel value="Male" control={<Radio />} label="Male" />
                                        <FormControlLabel value="Female" control={<Radio />} label="Female" />
                                    </RadioGroup>
                                </Grid>


                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        error={errors.firstName && touched.firstName}
                                        autoComplete="fname"
                                        name="firstName"
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        id="firstName"
                                        label="First Name"
                                        autoFocus
                                        helperText={
                                            errors.firstName && touched.firstName
                                                ? errors.firstName
                                                : null
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextField
                                        error={errors.lastName && touched.lastName}
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        id="lastName"
                                        label="Last Name"
                                        name="lastName"
                                        autoComplete="lname"
                                        helperText={
                                            errors.lastName && touched.lastName
                                                ? errors.lastName
                                                : null
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.email && touched.email}
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        id="email"
                                        label="Email Address"
                                        name="email"
                                        autoComplete="email"
                                        helperText={
                                            errors.email && touched.email ? errors.email : null
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.mobile && touched.mobile}
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        id="mobile"
                                        label="Mobile"
                                        name="mobile"
                                        autoComplete="mobile"
                                        helperText={
                                            errors.mobile && touched.mobile ? errors.mobile : null
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.password && touched.password}
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        name="password"
                                        label="Password"
                                        type="password"
                                        id="password"
                                        autoComplete="current-password"
                                        helperText={
                                            errors.password && touched.password
                                                ? errors.password
                                                : null
                                        }
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        error={errors.confirmPassword && touched.confirmPassword}
                                        variant="outlined"
                                        fullWidth
                                        onChange={handleChange}
                                        name="confirmPassword"
                                        label="Confirm Password"
                                        type="password"
                                        id="confirmPassword"
                                        autoComplete="current-password"
                                        helperText={
                                            errors.confirmPassword && touched.confirmPassword
                                                ? errors.confirmPassword
                                                : null
                                        }
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Sign Up
              </Button>
                        </Form>
                    )}
                </Formik>
            </div>
        </Container >
    );
});




















// import React from "react";
// import { Formik, Field, Form, ErrorMessage } from 'formik';
// import * as Yup from 'yup';
// import { makeStyles } from '@material-ui/core/styles';
// import {
//     TextField, Button, FormLabel, FormControlLabel, Grid, Container, FormControl, Paper, RadioGroup, Radio
// } from '@material-ui/core';
// import { AuthAction } from "../auth-action";
// import { AuthConsumer } from "../authentication-context-provider";
// import { withRouter } from 'react-router-dom';

// const useStyles = makeStyles(theme => ({
//     formControl: {
//         margin: theme.spacing(1),
//     },
//     Paper: {
//         background: "#3f51b536",
//     },
// }));


// export const Registration = withRouter(({ history, location }) => {


//     const classes = useStyles();

//     return (
//         <AuthConsumer>
//             {({ registrationDetails, updateRegistrationDetails }) => (
//                 <Formik
//                     initialValues={{
//                         firstName: registrationDetails.firstName,
//                         lastName: registrationDetails.lastName,
//                         email: registrationDetails.email,
//                         mobile: registrationDetails.mobile,
//                         password: registrationDetails.password,
//                         gender: registrationDetails.gender,
//                     }}
//                     validationSchema={Yup.object().shape({
//                         firstName: Yup.string()
//                             .required('First Name is required'),
//                         lastName: Yup.string()
//                             .required('Last Name is required'),
//                         email: Yup.string()
//                             .email('Email is invalid')
//                             .required('Email is required'),
//                         password: Yup.string()
//                             .min(6, 'Password must be at least 6 characters')
//                             .required('Password is required'),
//                         confirmPassword: Yup.string()
//                             .oneOf([Yup.ref('password'), null], 'Passwords must match')
//                             .required('Confirm Password is required'),
//                         mobile: Yup.string()
//                             .required('Mobile is required')
//                     })}
//                     onSubmit={(values, { props, setSubmitting }) => {
//                         alert("submit called");
//                         //alert('Registartion Details Submit Called!! :-)\n\n' + JSON.stringify(registrationDetails, null, 2));
//                     }}
//                     render={({ errors, status, touched }) => (

//                         <Container maxWidth="md">
//                             <Paper variant="outlined" className={classes.Paper}>
//                                 <Form>
//                                     <Grid container spacing={1}>
//                                         <Grid item xs={12}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <FormLabel component="legend">Gender</FormLabel>
//                                                 <RadioGroup
//                                                     //aria-label="flightType"
//                                                     name="genderType"
//                                                     //className={classes.group}
//                                                     value={registrationDetails.gender}
//                                                     aria-label="genderType"
//                                                     //onChange={formik.handleChange}
//                                                     onChange={event => { updateRegistrationDetails('gender', event.target.value) }}
//                                                     row>
//                                                     <FormControlLabel value="Male" control={<Radio />} label="Male" />
//                                                     <FormControlLabel value="Female" control={<Radio />} label="Female" />
//                                                 </RadioGroup>
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="firstName" name="firstName" label="First Name" type="search" variant="outlined" onChange={(e) => updateRegistrationDetails('firstName', e.target.value)}
//                                                     error={errors.firstName && touched.firstName}
//                                                     helperText={errors.firstName && touched.firstName ? errors.firstName : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>

//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="lastName" name="lastName" label="Last Name" type="search" variant="outlined" onChange={(e) => updateRegistrationDetails('lastName', e.target.value)}
//                                                     error={errors.lastName && touched.lastName}
//                                                     helperText={errors.lastName && touched.lastName ? errors.lastName : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="email" name="Email" label="Email" type="search" variant="outlined" onChange={(e) => updateRegistrationDetails('email', e.target.value)}
//                                                     error={errors.email && touched.email}
//                                                     helperText={errors.email && touched.email ? errors.email : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="mobile" name="mobile" label="Mobile" type="search" variant="outlined" onChange={(e) => updateRegistrationDetails('mobile', e.target.value)}
//                                                     error={errors.mobile && touched.mobile}
//                                                     helperText={errors.mobile && touched.mobile ? errors.mobile : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="password" name="password"
//                                                     label="Password" variant="outlined"
//                                                     onChange={(e) => updateRegistrationDetails('password', e.target.value)}
//                                                     error={errors.password && touched.password}
//                                                     helperText={errors.password && touched.password ? errors.password : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12} sm={6}>

//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <TextField id="standard-search" id="confirmPassword"
//                                                     name="confirmPassword"
//                                                     label="Confirm password" variant="outlined"
//                                                     error={errors.confirmPassword && touched.confirmPassword}
//                                                     helperText={errors.confirmPassword && touched.confirmPassword ? errors.confirmPassword : null} />
//                                             </FormControl>
//                                         </Grid>
//                                         <Grid item xs={12}>
//                                             <FormControl component="fieldset" className={classes.formControl}>
//                                                 <Button type="submit" fullWidth color="primary">Submit</Button>
//                                             </FormControl>
//                                         </Grid>
//                                     </Grid>
//                                 </Form>
//                             </Paper>
//                         </Container>
//                     )
//                     }
//                 />
//             )}
//         </AuthConsumer >
//     )

// })
